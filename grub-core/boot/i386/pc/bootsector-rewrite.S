# Boot.S rewritten -> BootSector.S


  **start**: // Bios -> Mbr boot entry
jmp  start_after_strings
MESSAGES

  **start_after_strings**:
%DL === %drivenum
setup_segments()
push %DX
%image_address_ptr = last_image_address

  **load_image_loop**:
%EAX:%EBX = qword [%image_address_ptr]
if %EAX:%EBX == 0:0  ->  jz  no_good_image_found
load_image()
check_signatures()  // jumpstarts loaded image, if accepted, otherwise returns
print_image_fail()

%image_address_ptr -= 8 byte
if %image_address_ptr >= first_image_address  ->  jae  load_image_loop

	**no_good_image_found**:
load_boot_partition()  // jumpstarts loaded image, if accepted, otherwise returns
int 18h
int 16h / cli ; hlt



setup_segments():
load_image():
check_signatures():
print_image_fail():
load_boot_partition():

