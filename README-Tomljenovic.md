*Archived from: http://915resolution.mango-lang.org/ (https://archive.vn/I0gI2, https://web.archive.org/web/20201201190342/http://915resolution.mango-lang.org/)*

<center>

# 915Resolution: Intel Video BIOS Hack  

Steve Tomljenovic \[stomljen at yahoo dot com\]  
Last modified April 26, 2007

</center>


915resolution is a tool to modify the video BIOS of the 800 and 900 series Intel graphics chipsets. This includes the 830, 845G, 855G, and 865G chipsets, as well as 915G, 915GM, 945G, 946GZ, G965, and Q965 chipsets. This modification is neccessary to allow the display of certain graphics resolutions for an Xorg or XFree86 graphics server.

915resolution's modifications of the BIOS are transient. There is no risk of permanent modification of the BIOS. This also means that 915resolution must be run every time the computer boots inorder for it's changes to take effect.

915resolution is derived from the tool [855resolution](http://perso.wanadoo.fr/apoirier/). However, the code differs substantially. 915resolution's code base is much simpler. 915resolution also allows the modification of bits per pixel.

915resolution has been reported to work with the following systems. It should, however, work with any system with an Intel 800 or 900 series graphics chipset.

| Make    | Series    | Model        |
|:-------:|:---------:|:------------:|
| Sony    | VAIO      | VGN-FS550    |
| Sony    | VAIO      | VGN-FS570    |
| Sony    | VAIO      | VGN-FS115B   |
| Sony    | VAIO      | VGN-FS115E   |
| IBM     | ThinkPad  | R52          |
| Dell    | Latitude  | X1           |
| Dell    | Latitude  | D610         |
| Samsung | X20       | XVM 1600 III |
| Asus    | -         | Z71A         |
| Asus    | -         | W5           |
| Acer    | Aspire    | 1691WLMi     |
| Toshiba | Satellite | M65          |


### Downloading 915resolution

You can download the current version of 915resolution, 0.5.3 [here](http://915resolution.mango-lang.org/download.html). This includes the source as well as a binary.

Debian packages can be found [here](http://www.freshnet.org/debian/hoary/).

You can find 915resolution scripts for Gentoo [here ](http://bugs.gentoo.org/show_bug.cgi?id=89820). There is also an [associated tarball](http://perso.numericable.fr/~bcolletb/915resolution.tar.bz2) for the Gentoo scripts.

FreeBSD users can look [here](http://www.freebsd.org/ports/sysutils.html) for a port of 915resolution.


### Using 915resolution

A README file with basic instructions on how to use 915resolution is included with the archive. You can also read it [here](http://915resolution.mango-lang.org/readme.html)

Note that you need to set the video BIOS with 915resolution before you start up the X server. You don't need to set them all. Just pick an unused mode with the right number of bits per pixel. Also, the setting is not permanent. You need to set the VBIOS every time you start the machine.

Be sure to the latest [drivers](http://support.intel.com/support/graphics/sb/CS-010512.htm) from Intel. You can also find an updated driver [here](http://www.fairlite.demon.co.uk/intel.html)

You also need at least X server version [6.8.2](http://www.x.org/X11R6.8.2/)

You'll need to fiddle with your X configuration to get things to work. You will probably need to create a modeline, and change horizontal/vertical scan frequencies. My X configuration file: [xorg.conf](http://915resolution.mango-lang.org/xorg_conf.html). If my modeline doesn't work for you, generate your own with [gtf](http://gtf.sourceforge.net/)

### Using 915resolution from within an Xserver

915resolution is going to be incorporated into the i810 driver from X.org. It should be in an upcoming release. This will void the need to run 915resolution manually. When I find out the appropriate X server has been released I'll put a notice on the website.

In the meantime, there is a test driver that has 915resolution built in [here](http://www.fairlite.demon.co.uk/intel.html)

The syntax to override a BIOS resolution is below. You place the option in the driver section.

<pre>Option "ForceBIOS" "1024x768=1400x1050"
</pre>

This will re-program the old 1024x768 to become a new 1400x1050 one.

### Helpful links

There is a google groups forum [915resolution](http://groups.google.com/group/915resolution). If your having problems or have any questions, please post there. You can also email me (Steve Tomljenovic) at [stomljen at yahoo com].

You can find discussion about this problem within x.org's bug tracking system. It is [bug 643](https://bugs.freedesktop.org/show_bug.cgi?id=643)

[X.org wiki](http://wiki.x.org/wiki/IntelGraphicsDriver)

An interesting set of links on using centrino notebooks can be found [here](http://tuxmobil.org/centrino.html)

Below are numerous links of people reporting thier experiences with 915resolution. This is a good place to look if your having problems..

English:

[https://wiki.ubuntu.com/i915Driver](https://wiki.ubuntu.com/i915Driver)

[http://en.opensuse.org/Patch_the_Video_BIOS](http://en.opensuse.org/Patch_the_Video_BIOS)

[http://roland-lopez.blogspot.com/2007/03/auto915resolution-ubuntu-resolution-fix.html](http://roland-lopez.blogspot.com/2007/03/auto915resolution-ubuntu-resolution-fix.html)

[http://www.linuxquestions.org/linux/answers/Hardware/Using_915resolution_to_Support_Wide_Aspect_Displays_with_Intel_Extreme_Graphics_Cards](http://www.linuxquestions.org/linux/answers/Hardware/Using_915resolution_to_Support_Wide_Aspect_Displays_with_Intel_Extreme_Graphics_Cards)

[http://absolutebeginner.wordpress.com/2006/08/20/absolute-beginner-guide-915resolution/](http://absolutebeginner.wordpress.com/2006/08/20/absolute-beginner-guide-915resolution/)

[http://blogs.warwick.ac.uk/atrivedi/entry/sony_vaio_vgn/](http://blogs.warwick.ac.uk/atrivedi/entry/sony_vaio_vgn/)

[http://blogs.warwick.ac.uk/atrivedi/entry/using_915resolution_reply/](http://blogs.warwick.ac.uk/atrivedi/entry/using_915resolution_reply/)

[http://gentoo-wiki.com/HARDWARE_Dell_Latitude_X1](http://gentoo-wiki.com/HARDWARE_Dell_Latitude_X1)

[http://surrey.lug.org.uk/cgi-bin/wiki.pl?TravelMate4100WLMi](http://surrey.lug.org.uk/cgi-bin/wiki.pl?TravelMate4100WLMi)

[http://www.technodaddy.com/wp/2005/04/30/i-am-loving-ubuntu-linux/](http://www.technodaddy.com/wp/2005/04/30/i-am-loving-ubuntu-linux/)

[http://www.stud.ntnu.no/~gronslet/blog/linux-on-a-dell-x1-aka-samsung-q30](http://www.stud.ntnu.no/~gronslet/blog/linux-on-a-dell-x1-aka-samsung-q30)

[http://home.comcast.net/~canez/d610/](http://home.comcast.net/~canez/d610/)

[http://jastram.de/story.php?id=185](http://jastram.de/story.php?id=185)

[http://www.zvolve.com/~garry/lappy.html](http://www.zvolve.com/~garry/lappy.html)

[http://www.baycom.org/~tom/acertm3004wtmi/](http://www.baycom.org/~tom/acertm3004wtmi/)

[http://www.linux-laptop.net/hosted/fs215e-slackware.html](http://www.linux-laptop.net/hosted/fs215e-slackware.html)

[http://www.bram.be/travelmate_4651lci.html](http://www.bram.be/travelmate_4651lci.html)

[http://tuxmobil.org/centrino.html](http://tuxmobil.org/centrino.html)

[http://z63a.koneko.org/fc4-z63a.html](http://z63a.koneko.org/fc4-z63a.html)

[http://users.skynet.be/thomasvst/linux-on-laptop/](http://users.skynet.be/thomasvst/linux-on-laptop/)

[http://rdo.homelinux.org/ubuntu-linux-on-a-dell-latitude-d610/](http://rdo.homelinux.org/ubuntu-linux-on-a-dell-latitude-d610/)

[http://spide.blogspot.com/2005/08/softwarerec-install-ubuntu-on-acer.html](http://spide.blogspot.com/2005/08/softwarerec-install-ubuntu-on-acer.html)

[http://www.nerdgirl.dk/linux/installation.php#monitor](http://www.nerdgirl.dk/linux/installation.php#monitor)

[http://mandrivaonadellx1.50webs.com/](http://mandrivaonadellx1.50webs.com/)

German:

[http://www.ubuntuusers.de/wiki/wiki_und_community:testberichte:dell_latitude_x1](http://www.ubuntuusers.de/wiki/wiki_und_community:testberichte:dell_latitude_x1)

French:

[http://poseidon.epfl.ch/page6782.html](http://poseidon.epfl.ch/page6782.html)

Italian:

[http://www.traindevie.com/w5a/w5a.html](http://www.traindevie.com/w5a/w5a.html)

