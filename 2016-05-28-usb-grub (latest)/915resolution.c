/* 915resolution - Utility to change vbemodes on the intel
 * integrated video chipset */

/*
 * Based on Nathan Coulson's http://nathancoulson.com/proj/eee/grub-1.96-915resolution-0.5.2-3.patch
 * Oct 10, 2008, Released as 915
 * Oct 10, 2008, Updated to include support for 945GM thanks to Scot Doyle
 * original command-line tool from 2007 by Steve Tomljenovic:  http://915resolution.mango-lang.org/  (not maintained)
 * there's no central repo for 915res development, the most complete versions in 2016-05 are:
 * - this grub module at:  http://git.savannah.gnu.org/cgit/grub-extras.git/tree/915resolution/915resolution.c
 * - tempestous' version for Puppy Linux at:  http://www.murga-linux.com/puppy/viewtopic.php?mode=attach&id=72709  --  915resolution-0.5.3-patched20120521.tar.gz
 * -> see source-links.md for the various patches and versions found
 */

/* Copied from 915 resolution created by steve tomjenovic
 * 915 resolution was in the public domain.
 * 
 * All I have done, was make the above program run within 
 * the grub2 environment.  
 *
 * Some of the checks are still commented, as I did not find
 * easy replacement for memmem.
 *
 * Slightly edited by Nathan Coulson (conathan@gmail.com)
 */

/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2003,2007  Free Software Foundation, Inc.
 *  Copyright (C) 2003  NIIBE Yutaka <gniibe@m17n.org>
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */


/* 915 resolution by steve tomljenovic
 *
 * This was tested only on Sony VGN-FS550.  Use at your own risk
 *
 * This code is based on the techniques used in :
 *
 *   - 855patch.  Many thanks to Christian Zietz (czietz gmx net)
 *     for demonstrating how to shadow the VBIOS into system RAM
 *     and then modify it.
 *
 *   - 1280patch by Andrew Tipton (andrewtipton null li).
 *
 *   - 855resolution by Alain Poirier
 *
 * This source code is into the public domain.
 */

#include <grub/types.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/err.h>
#include <grub/dl.h>
#include <grub/normal.h>
#include <grub/i386/io.h>
#include <grub/video.h>

GRUB_MOD_LICENSE ("GPLv3+");

#define printf					grub_printf
#define malloc					grub_malloc
#define free					grub_free
#define strlen					grub_strlen
#define strcmp					grub_strcmp
#define strchr					grub_strchr
#define fprintf(stream, ...)			grub_printf(__VA_ARGS__)
#define strtol(x,y,z)				grub_strtoul(x,y,z)
#define atoi(x)					grub_strtoul(x,NULL,10)
#define assert(x)				
#define memset					grub_memset
#define outl		grub_outl
#define outb		grub_outb
#define inl		grub_inl
#define inb		grub_inb

#define NEW(a) ((a *)(malloc(sizeof(a))))
#define FREE(a) (free(a))

#define VBIOS_START         0xc0000
#define VBIOS_SIZE          0x10000

#define VBIOS_FILE    "/dev/mem"

#define FALSE 0
#define TRUE 1

#define MODE_TABLE_OFFSET_845G 617

#define RES915_VERSION "0.5.3"

#define ATI_SIGNATURE1 "ATI MOBILITY RADEON"
#define ATI_SIGNATURE2 "ATI Technologies Inc"
#define NVIDIA_SIGNATURE "NVIDIA Corp"
#define INTEL_SIGNATURE "Intel Corp"

#define DEBUG 0

typedef unsigned char * address;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned char boolean;
typedef unsigned int cardinal;

typedef enum {
    BT_UNKWN, BT_1=1, BT_2=2, BT_3=3
} bios_type;


int freqs[] = { 60, 75, 85 };

typedef struct {
    byte mode;
    byte bits_per_pixel;
    word resolution;
    byte unknown;
} __attribute__((packed)) vbios_mode;

typedef struct {
    byte unknow1[2];
    byte x1;
    byte x_total;
    byte x2;
    byte y1;
    byte y_total;
    byte y2;
} __attribute__((packed)) vbios_resolution_type1;

typedef struct {
    unsigned long clock;

    word x1;
    word htotal;
    word x2;
    word hblank;
    word hsyncstart;
    word hsyncend;

    word y1;
    word vtotal;
    word y2;
    word vblank;
    word vsyncstart;
    word vsyncend;
} __attribute__((packed)) vbios_modeline_type2;

typedef struct {
    byte xchars;
    byte ychars;
    byte unknown[4];

    vbios_modeline_type2 modelines[];
} __attribute__((packed)) vbios_resolution_type2;

typedef struct {
    unsigned long clock;

    word x1;
    word htotal;
    word x2;
    word hblank;
    word hsyncstart;
    word hsyncend;

    word y1;
    word vtotal;
    word y2;
    word vblank;
    word vsyncstart;
    word vsyncend;

    word timing_h;
    word timing_v;

    byte unknown[6];
} __attribute__((packed)) vbios_modeline_type3;

typedef struct {
    unsigned char unknown[6];

    vbios_modeline_type3 modelines[];
} __attribute__((packed)) vbios_resolution_type3;


struct device_descriptor_t;

typedef struct {
    // after grub-2.02-beta3: renamed from  chipset_id  to  device_id
    cardinal device_id;
    const struct device_descriptor_t* chipset;
    bios_type bios;
    
    address bios_ptr;

    vbios_mode * mode_table;
    cardinal mode_table_size;

    byte b1, b2;

    boolean unlocked;
} vbios_map;


typedef struct {
  cardinal x, y;
} resolution_t;


typedef struct {
  cardinal quiet, list, raw;
  cardinal mode;
  cardinal x, y, bp;
  cardinal htotal, vtotal;
  const char* forced_chipset;
} parameters_t;

parameters_t  parameters;


/*
DECLARE_CHIPSET(830)
DECLARE_CHIPSET(845G)
DECLARE_CHIPSET(855GM)
DECLARE_CHIPSET(865G)
DECLARE_CHIPSET(915G)
DECLARE_CHIPSET(915GM)
DECLARE_CHIPSET(945G)
DECLARE_CHIPSET(945GM)
DECLARE_CHIPSET(945GME)
DECLARE_CHIPSET(946GZ)
DECLARE_CHIPSET(G965)
DECLARE_CHIPSET(Q965)
DECLARE_CHIPSET(965GM)
DECLARE_CHIPSET(G33)
DECLARE_CHIPSET(Q33)
DECLARE_CHIPSET(Q35)
DECLARE_CHIPSET(500GMA)
DECLARE_CHIPSET(GM45)
DECLARE_CHIPSET(GMA3150)
DECLARE_CHIPSET(HD3000)
DECLARE_CHIPSET(HDGraphics)
*/

typedef void (vbios_lock_func_t) (vbios_map * map, cardinal relock);
static vbios_lock_func_t  vbios_lock_830_855GM;
static vbios_lock_func_t  vbios_lock_default;

typedef struct device_descriptor_t {
  cardinal  device_id;
  const char*  chipset_name;
  vbios_lock_func_t*  vbios_lock_func;
} device_descriptor_t;

#define DECLARE_DEVICE( id, name )  { id, #name, vbios_lock_default },
#define DECLARE_DEVICF( id, name, lock_func )  { id, #name, lock_func },

static device_descriptor_t device_descriptors[] = {
DECLARE_DEVICF( 0x35758086 , 830 , vbios_lock_830_855GM )
DECLARE_DEVICE( 0x25608086 , 845G )
DECLARE_DEVICF( 0x35808086 , 855GM , vbios_lock_830_855GM )
DECLARE_DEVICE( 0x25708086 , 865G )
DECLARE_DEVICE( 0x25808086 , 915G )
DECLARE_DEVICE( 0x25908086 , 915GM )
DECLARE_DEVICE( 0x27708086 , 945G )
DECLARE_DEVICE( 0x27a08086 , 945GM )
DECLARE_DEVICE( 0x27ac8086 , 945GME )
DECLARE_DEVICE( 0x29708086 , 946GZ )
DECLARE_DEVICE( 0x29a08086 , G965 )
DECLARE_DEVICE( 0x29908086 , Q965 )
DECLARE_DEVICE( 0x2a008086 , 965GM )
DECLARE_DEVICE( 0x2a018086 , 965GM )
DECLARE_DEVICE( 0x2a028086 , 965GM )
DECLARE_DEVICE( 0x29c08086 , G33 )
DECLARE_DEVICE( 0x29d08086 , Q33 )
DECLARE_DEVICE( 0x29b08086 , Q35 )
DECLARE_DEVICE( 0x81008086 , 500GMA )
DECLARE_DEVICE( 0x2a408086 , GM45 )
DECLARE_DEVICE( 0xa0008086 , GMA3150 )
DECLARE_DEVICE( 0xa0108086 , GMA3150 )
DECLARE_DEVICE( 0x01048086 , HD3000 )
DECLARE_DEVICE( 0x00408086 , HDGraphics )
DECLARE_DEVICE( 0x00448086 , HDGraphics )
};



static  const device_descriptor_t*  get_chipset(cardinal device_id) {
    for (int i=0, count=sizeof(device_descriptors)/sizeof(device_descriptors[0]); i < count; i++) {
      const device_descriptor_t* desc = &device_descriptors[i];
      if (desc->device_id == device_id)  return desc;
    }
    return NULL;
}

static  const device_descriptor_t*  get_chipset_by_name(const char* chipset_name) {
    for (int i=0, count=sizeof(device_descriptors)/sizeof(device_descriptors[0]); i < count; i++) {
      const device_descriptor_t* desc = &device_descriptors[i];
      if ( ! strcmp(desc->chipset_name, chipset_name) )  return desc;
    }
    return NULL;
}



// after grub-2.02-beta3: renamed from  get_chipset_id()  to  get_device_id()
static cardinal get_device_id(void) {
    outl(0x80000000, 0xcf8);
    return inl(0xcfc);
}


static void vbios_lock_default(vbios_map * map, cardinal relock) {
  if (! relock) {
    outl(0x80000090, 0xcf8);
    map->b1 = inb(0xcfd);
    map->b2 = inb(0xcfe);
    
    outl(0x80000090, 0xcf8);
    outb(0x33, 0xcfd);
    outb(0x33, 0xcfe);
  }
  else {
    outl(0x80000090, 0xcf8);
    outb(map->b1, 0xcfd);
    outb(map->b2, 0xcfe);
  }
}


static void vbios_lock_830_855GM(vbios_map * map, cardinal relock) {
  if (! relock) {
    outl(0x8000005a, 0xcf8);
    map->b1 = inb(0xcfe);
    
    outl(0x8000005a, 0xcf8);
    outb(0x33, 0xcfe);
  }
  else {
    outl(0x8000005a, 0xcf8);
    outb(map->b1, 0xcfe);
  }
}


static void vbios_lock(vbios_map * map, cardinal relock) {
  assert(!map->unlocked == !relock);
  map->unlocked = !relock;
  
  vbios_lock_func_t*  vbios_lock_func= map->chipset->vbios_lock_func ? : vbios_lock_default;
  vbios_lock_func(map, relock);
  
  #if DEBUG
  if (!relock)
  {
    cardinal t = inl(0xcfc);
    printf("unlock PAM: (0x%08x)\n", t);
  }
  else {
    cardinal t = inl(0xcfc);
    printf("relock PAM: (0x%08x)\n", t);
  }
  #endif
}



static void init_chipset(vbios_map * map, const char* forced_chipset) {
    if (forced_chipset) {
      map->chipset = get_chipset_by_name(forced_chipset);
    }
    else {
      // Determine chipset
      map->device_id = get_device_id();
      map->chipset = get_chipset(map->device_id);
    }
    
    // Map the video bios to memory
    map->bios_ptr = (unsigned char *) VBIOS_START;

#if 0
    /*
     * check if we have ATI Radeon
     */
    
    if (memmem(map->bios_ptr, VBIOS_SIZE, ATI_SIGNATURE1, strlen(ATI_SIGNATURE1)) ||
        memmem(map->bios_ptr, VBIOS_SIZE, ATI_SIGNATURE2, strlen(ATI_SIGNATURE2)) ) {
      if ( ! parameters.quiet ) {
        fprintf(stderr, "ATI chipset detected.  915resolution only works with Intel 800/900 series graphic chipsets.\n");
      }
      return 0;
    }

    /*
     * check if we have NVIDIA
     */
    
    if (memmem(map->bios_ptr, VBIOS_SIZE, NVIDIA_SIGNATURE, strlen(NVIDIA_SIGNATURE))) {
      if ( ! parameters.quiet ) {
        fprintf(stderr, "NVIDIA chipset detected.  915resolution only works with Intel 800/900 series graphic chipsets.\n");
      }
      return 0;
    }

    /*
     * check if we have Intel
     */
    
    if (map->chipset == CT_UNKWN && memmem(map->bios_ptr, VBIOS_SIZE, INTEL_SIGNATURE, strlen(INTEL_SIGNATURE))) {
      if ( ! parameters.quiet ) {
        fprintf(stderr, "Intel chipset detected.  However, 915resolution was unable to determine the chipset type.\n");
        fprintf(stderr, "Device Id: %x\n", map->device_id);
        fprintf(stderr, "Please report this problem to stomljen@yahoo.com\n");
      }
      return 0;
    }
#endif

    if (map->chipset == NULL) {
      if ( ! parameters.quiet ) {
        fprintf(stderr, "Unknown chipset type and unrecognized bios.\n");
        fprintf(stderr, "915resolution only works with Intel 800/900 series graphic chipsets.\n");
        fprintf(stderr, "If you think your card has a similar bios to Intel 915,\n");
        fprintf(stderr, "you may try running  915resolution -c 915GM -l  to list the found video modes.\n");
        fprintf(stderr, "May freeze your computer... If not, and the video modes are as expected,\n");
        fprintf(stderr, "Try  915resolution -c 915GM auto\n");
        fprintf(stderr, "Device Id: %x\n", map->device_id);
      }
    }
}

static void init_mode_table(vbios_map * map) {
        // Figure out where the mode table is 
        address p = map->bios_ptr + 16;
        address limit = map->bios_ptr + VBIOS_SIZE - (3 * sizeof(vbios_mode));
        
        while (p < limit && map->mode_table == 0) {
            vbios_mode * mode_ptr = (vbios_mode *) p;
            
            if (((mode_ptr[0].mode & 0xf0) == 0x30) && ((mode_ptr[1].mode & 0xf0) == 0x30) &&
                ((mode_ptr[2].mode & 0xf0) == 0x30) && ((mode_ptr[3].mode & 0xf0) == 0x30)) {

                map->mode_table = mode_ptr;
            }
            
            p++;
        }

        if (map->mode_table) {
            // Determine size of mode table
            vbios_mode * mode_ptr = map->mode_table;
            
            while (mode_ptr->mode != 0xff) {
                map->mode_table_size++;
                mode_ptr++;
            }
        }
        else if ( ! parameters.quiet ) {
            fprintf(stderr, "Unable to locate the mode table.\n");
            fprintf(stderr, "Please run the program 'dump_bios' as root and\n");
            fprintf(stderr, "email the file 'vbios.dmp' to stomljen@yahoo.com.\n");
            fprintf(stderr, "Chipset: %s\n", map->chipset ? map->chipset->chipset_name : "unknown");
        }
}


static short int detect_resolution_diff(vbios_map * map) {
    unsigned i;
    short int r1, r2;
    
    r1 = r2 = 32000;

    for (i=0; i < map->mode_table_size; i++) {
      word r= map->mode_table[i].resolution;
      if (r <= r1) {
        // ??? r2= r1;  to save second smallest?
        r1 = r;
      }
      else if (r <= r2) {
        r2 = r;
      }

      /*printf("r1 = %d  r2 = %d\n", r1, r2);*/
    }

    return (r2-r1-6);
}

static void init_bios_type(vbios_map * map) {
    /*
     * Figure out what type of bios we have
     *  order of detection is important
     */
     
    short int diff= detect_resolution_diff(map);
    
    if (0 == diff % sizeof(vbios_modeline_type3))  map->bios = BT_3;
    else if (0 == diff % sizeof(vbios_modeline_type2))  map->bios = BT_2;
    else if (0 == diff % sizeof(vbios_resolution_type1))  map->bios = BT_1;
    else if ( ! parameters.quiet ) {
        fprintf(stderr, "Unable to determine bios type.\n");
        fprintf(stderr, "Please run the program 'dump_bios' as root and\n");
        fprintf(stderr, "email the file 'vbios.dmp' to stomljen@yahoo.com.\n");
    }
}



static resolution_t get_mode_resolution_type1(const vbios_map * map, cardinal mode_index) {
  resolution_t  r;
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type1 * res = ((vbios_resolution_type1*)(map->bios_ptr + mode_res_offs)); 
  r.x = ((((cardinal) res->x2) & 0xf0) << 4) | res->x1;
  r.y = ((((cardinal) res->y2) & 0xf0) << 4) | res->y1;
  return r;
}

static resolution_t get_mode_resolution_type2(const vbios_map * map, cardinal mode_index) {
  resolution_t  r;
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type2 * res = ((vbios_resolution_type2*)(map->bios_ptr + mode_res_offs)); 
  r.x = res->modelines[0].x1+1;
  r.y = res->modelines[0].y1+1;
  return r;
}

static resolution_t get_mode_resolution_type3(const vbios_map * map, cardinal mode_index) {
  resolution_t  r;
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type3 * res = ((vbios_resolution_type3*)(map->bios_ptr + mode_res_offs)); 
  r.x = res->modelines[0].x1+1;
  r.y = res->modelines[0].y1+1;
  return r;
}

typedef resolution_t (get_mode_res_func_t) (const vbios_map * map, cardinal mode_index);
get_mode_res_func_t*  get_mode_resolution_funcs[] = {
  NULL,     // [0]
  get_mode_resolution_type1,  // [1]
  get_mode_resolution_type2,  // [2]
  get_mode_resolution_type3,  // [3]
  NULL
};



static void get_last_mode(const vbios_map* map, cardinal* mode) {
  get_mode_res_func_t*  get_mode_res_func = get_mode_resolution_funcs[map->bios];
  if ( ! get_mode_res_func )  return;
  
  if ( ! map->mode_table_size )  return;
  
  cardinal  mode_ret = map->mode_table[0].mode;
  for (cardinal i = 0; i < map->mode_table_size; i++) {
    resolution_t res = get_mode_res_func(map, i);
    if (res.x != 0 && res.y != 0) {
      mode_ret = map->mode_table[i].mode;
    }
  }
  *mode= mode_ret;
}


static void list_modes(const vbios_map *map) {
    get_mode_res_func_t*  get_mode_res_func = get_mode_resolution_funcs[map->bios];
    if ( ! get_mode_res_func )  return;

    for (cardinal i=0; i < map->mode_table_size; i++) {
      resolution_t  r= get_mode_res_func(map, i);
      if (r.x != 0 && r.y != 0) {
        printf("Mode %02x: %dx%d, %d bits/pixel\n", map->mode_table[i].mode, r.x, r.y, map->mode_table[i].bits_per_pixel);
      }
      if (parameters.raw && map->bios == 1) {
        word  mode_res_offs = map->mode_table[i ].resolution;
        const vbios_resolution_type1 *  res = ((const vbios_resolution_type1*)(map->bios_ptr + mode_res_offs)); 
        printf("Mode %02x (raw): unknow1[2]= %02x %02x  x1,x_total,x2= %02x %02x %02x  y1,y_total,y2= %02x %02x %02x\n", map->mode_table[i].mode,
          res->unknow1[0],res->unknow1[1],
          res->x1,res->x_total,res->x2,
          res->y1,res->y_total,res->y2);
      }
    }
}



static void display_map_info(const vbios_map * map) {
    printf("Chipset: %s\n", map->chipset ? map->chipset->chipset_name : "unknown");
    if (map->bios)  printf("BIOS: TYPE %d\n", (int)map->bios);
    else  printf("BIOS: TYPE unknown\n");

    if (map->mode_table)  printf("Mode Table Offset: $C0000 + $%x\n", ((cardinal)map->mode_table) - ((cardinal)map->bios_ptr));
    else  printf("Mode Table Offset: unknown\n");
    printf("Mode Table Entries: %u\n", map->mode_table_size);
}



static void gtf_timings(int x, int y, int freq,
        unsigned long *clock,
        word *hsyncstart, word *hsyncend, word *hblank,
        word *vsyncstart, word *vsyncend, word *vblank)
{
    int hbl, vbl, vfreq;

    /*
      (y+1)/(20000.0/(11*freq) - 1) + 0.5
      = ((y+1) * (11*freq))/(20000.0 - (11*freq)) + 0.5
      = ((y+1) * (11*freq) + 10000.0 - (11*freq) / 2)/(20000.0 - (11*freq))
      = ((y+1) * (11*freq) + 10000.0 - 5 * freq - (freq + 1) / 2)/(20000.0 - (11*freq))
*/
    vbl = y + 1 +
      grub_divmod64 (((y+1) * (11*(long long)freq) + 10000 - 5 * (long long)freq
		      - (freq + 1) / 2),
		     (20000 - (11*(long long)freq)), 0);
    vfreq = vbl * freq;
    /*
      (x * (30.0 - 300000.0 / vfreq) /
      (70.0 + 300000.0 / vfreq) / 16.0 + 0.5)
      = ((x * (30.0 - 300000.0 / vfreq) /
          (70.0 + 300000.0 / vfreq) + 8) / 16)
      = ((x * (30.0 * vfreq - 300000.0) /
          (70.0 * vfreq + 300000.0) + 8) / 16)
      = (((x * (30 * vfreq - 300000)) /
          (70 * vfreq + 300000) + 8) / 16)
    */
    hbl = 16 * (int)((grub_divmod64((x * (30 * (long long)vfreq - 300000)),
				    (70 * (long long)vfreq + 300000), 0)
		      + 8) / 16);

    *vsyncstart = y;
    *vsyncend = y + 3;
    *vblank = vbl - 1;
    *hsyncstart = x + hbl / 2 - (x + hbl + 50) / 100 * 8 - 1;
    *hsyncend = x + hbl / 2 - 1;
    *hblank = x + hbl - 1;
    *clock = (x + hbl) * vfreq / 1000;
}


static void set_mode_resolution_type1(vbios_map * map, cardinal mode_index, const parameters_t * p) {
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type1 *  res = ((vbios_resolution_type1*)(map->bios_ptr + mode_res_offs)); 
  
  if (p->bp)  map->mode_table[mode_index].bits_per_pixel = p->bp;
  
  res->x2 = ( p->htotal ? (((p->htotal - p->x) >> 8) & 0x0f) : (res->x2 & 0x0f) )  |  ( (p->x >> 4) & 0xf0 );
  res->x1 = (p->x & 0xff);
  
  res->y2 = ( p->vtotal ? (((p->vtotal - p->y) >> 8) & 0x0f) : (res->y2 & 0x0f) )  |  ( (p->y >> 4) & 0xf0 );
  res->y1 = (p->y & 0xff);
  
  if (p->htotal)  res->x_total = ((p->htotal - p->x) & 0xff);
  if (p->vtotal)  res->y_total = ((p->vtotal - p->y) & 0xff);
}

static void set_mode_resolution_type2(vbios_map * map, cardinal mode_index, const parameters_t * p) {
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type2 *  res = ((vbios_resolution_type2*)(map->bios_ptr + mode_res_offs)); 
  
  // forgot about .bits_per_pixel ?
  //if (p->bp)  map->mode_table[mode_index].bits_per_pixel = p->bp;
  
  cardinal  xprev, yprev, j;
  res->xchars = p->x / 8;
  res->ychars = p->y / 16 - 1;
  xprev = res->modelines[0].x1;
  yprev = res->modelines[0].y1;
  
  for(j=0; j < 3; j++) {
    vbios_modeline_type2 * modeline = &res->modelines[j];
    
    if (modeline->x1 == xprev && modeline->y1 == yprev) {
      modeline->x1 = modeline->x2 = p->x - 1;
      modeline->y1 = modeline->y2 = p->y - 1;

      gtf_timings(p->x, p->y, freqs[j], &modeline->clock,
              &modeline->hsyncstart, &modeline->hsyncend,
              &modeline->hblank, &modeline->vsyncstart,
              &modeline->vsyncend, &modeline->vblank);
      modeline->htotal = p->htotal ? : modeline->hblank;
      modeline->vtotal = p->vtotal ? : modeline->vblank;
    }
  }
}

static void set_mode_resolution_type3(vbios_map * map, cardinal mode_index, const parameters_t * p) {
  word  mode_res_offs = map->mode_table[mode_index].resolution;
  vbios_resolution_type3 *  res = ((vbios_resolution_type3*)(map->bios_ptr + mode_res_offs)); 
  
  // forgot about .bits_per_pixel ?
  //if (p->bp)  map->mode_table[mode_index].bits_per_pixel = p->bp;
  
  cardinal  xprev, yprev, j;
  xprev = res->modelines[0].x1;
  yprev = res->modelines[0].y1;
  
  for (j=0; j < 3; j++) {
    vbios_modeline_type3 * modeline = &res->modelines[j];
    
    if (modeline->x1 == xprev && modeline->y1 == yprev) {
        modeline->x1 = modeline->x2 = p->x - 1;
        modeline->y1 = modeline->y2 = p->y - 1;
        
        gtf_timings(p->x, p->y, freqs[j], &modeline->clock,
                &modeline->hsyncstart, &modeline->hsyncend,
                &modeline->hblank, &modeline->vsyncstart,
                &modeline->vsyncend, &modeline->vblank);
        modeline->htotal = p->htotal ? : modeline->hblank;
        modeline->vtotal = p->vtotal ? : modeline->vblank;
        modeline->timing_h   = p->y - 1;
        modeline->timing_v   = p->x - 1;
    }
  }
}

typedef void (set_mode_res_func_t) (vbios_map * map, cardinal mode_index, const parameters_t * p);
set_mode_res_func_t*  set_mode_resolution_funcs[]=  {
  NULL,    // [0]
  set_mode_resolution_type1,  // [1]
  set_mode_resolution_type2,  // [2]
  set_mode_resolution_type3,  // [3]
  NULL
};

static void set_mode(vbios_map * map, const parameters_t * p) {
  set_mode_res_func_t*  set_mode_res_func=  set_mode_resolution_funcs[map->bios];
  if ( ! set_mode_res_func )  return;

  for (cardinal i=0; i < map->mode_table_size; i++) {
    if (map->mode_table[i].mode == p->mode) {
      set_mode_res_func(map, i, p);
    }
  }
}



static void get_preferred_res(cardinal* x, cardinal* y) {
  #ifdef GRUB_MACHINE_PCBIOS
  grub_dl_load ("vbe");
  #endif
  
  grub_video_adapter_t adapter;
  FOR_VIDEO_ADAPTERS (adapter)
  {
    #ifdef GRUB_VIDEO_ADAPTER_GET_PREFERRED_MODE
    if (adapter->get_preferred_mode
    &&  GRUB_ERR_NONE == adapter->get_preferred_mode(x, y) ) {
      if ( ! parameters.quiet )  printf("915resolution: Video adapter %s reported preferred mode %ux%u.", adapter->name, *x, *y);
      return;
    }
    #endif
    
    struct grub_video_edid_info edid_info;
    if (adapter->get_edid
    &&  GRUB_ERR_NONE == adapter->get_edid (&edid_info)
    &&  GRUB_ERR_NONE == grub_video_edid_checksum (&edid_info)
    &&  GRUB_ERR_NONE == grub_video_edid_preferred_mode (&edid_info, x, y) ) {
      if ( ! parameters.quiet )  printf("915resolution: Video adapter %s reported EDID preferred mode %ux%u.", adapter->name, *x, *y);
      return;
    }
    //grub_errno = GRUB_ERR_NONE;
  }
  if ( ! parameters.quiet )  printf("915resolution: None of the video adapters reported EDID preferred mode.");
}



static int parse_args(cardinal argc, char *argv[], parameters_t *p) {
    cardinal index = 0;

    if ((argc > index) && !strcmp(argv[index], "-q")) {
        p->quiet = 1;
        index++;
    }    

    if ((argc > index) && !strcmp(argv[index], "-c")) {
        index++;
        if(argc<=index)  return 1;
        
        p->forced_chipset= argv[index];
        index++;
    }

    if ((argc > index) && !strcmp(argv[index], "-l")) {
        p->list = 1;
        index++;
    }
    
    if ((argc > index) && !strcmp(argv[index], "-r")) {
        p->raw = 1;
        index++;
    }
    
    // get_last_mode() will set mode if not set here
    if ((argc > index) && 2 == strlen(argv[index])) {
      p->mode = (cardinal) strtol(argv[index], NULL, 16);
      index++;
    }

    if(argc<=index)  return 0;
    
    const char * argvx= NULL;
    if (!strcmp(argv[index], "auto")) {
      // get_preferred_res() will set x,y
      p->x= 0;
      p->y= 1;
      
      index++;
    }
    else if ( NULL != (argvx = strchr(argv[index], 'x')) ) {
      p->x= (cardinal)strtol(argv[index], NULL, 10);
      p->y= (cardinal)strtol(argvx+1, NULL, 10);
      if ( NULL != (argvx = strchr(argvx+1, 'x')) ) {
        p->bp= (cardinal)strtol(argvx+1, NULL, 10);
      }
      index++;
    }
    else {
      if (argc-index < 2)  return 1;

      p->x= (cardinal)atoi(argv[index]);
      index++;
      p->y= (cardinal)atoi(argv[index]);
      index++;
      
      if (argc > index)  { p->bp = (cardinal)atoi(argv[index]);  index++; }
    }

    if (argc > index)  { p->htotal = (cardinal)atoi(argv[index]);  index++; }
    if (argc > index)  { p->vtotal = (cardinal)atoi(argv[index]);  index++; }
    
    return 0;
}



static void usage(void) {
    printf("For widescreen laptops with intel graphics:\n");
    printf("add missing native lcd resolution to vesa bios mode table  detected from monitor EDID\n");
    printf("Default usage in grub.cfg accepts values in the format of $gfxmode:\n");
    printf("  915resolution -q $gfxmode\n");
    printf("    -q : optional, quiet mode to use in script -- no message displayed if:\n");
    printf("         there's no intel graphics chipset  or  the display does not provide native resolution\n");
    printf("Set last video mode to preferred resolution:\n");
    printf("  915resolution [-q] auto\n");
    printf("Set last video mode to specific resolution:\n");
    printf("  915resolution [-q] XxY\n");
    printf("    XxY : WIDTHxHEIGHT (eg. 1680x1050)\n");
    printf("  915resolution [-q] XxYxBITS\n");
    printf("    XxYxBITS : WIDTHxHEIGHTxBITS (eg. 1920x1200x32)\n");
    printf("Display list of modes found in the video BIOS:\n");
    printf("  915resolution -l\n");
    printf("Advanced usage:\n");
    printf("  915resolution [-q] [-c chipset] [-l] [-r] [mode] [XxY[xBITS] | X Y [bits/pixel]] [htotal] [vtotal]\n");
    printf("  Set the resolution to XxY for a video mode\n");
    printf("  Bits per pixel are optional.  htotal/vtotal settings are additionally optional.\n");
    printf("  Options:\n");
    printf("    -q don't show any normal messages\n");
    printf("    -c force chipset type (THIS IS USED FOR DEBUG PURPOSES)\n");
    printf("    -l display the modes found in the video BIOS\n");
    printf("    -r display the modes found in the video BIOS in raw mode (THIS IS USED FOR DEBUG PURPOSES)\n");
}

static int main_915 (int argc, char *argv[])
{
    vbios_map  map;
    memset (&map, 0, sizeof(map));
    memset (&parameters, 0, sizeof(parameters));
    
    if ( parse_args(argc, argv, &parameters) ) {
        printf("Intel 800/900 Series VBIOS Hack : version %s\n", RES915_VERSION);
        usage();
        return 2;
    }

    if ( ! parameters.quiet )  printf("Intel 800/900 Series VBIOS Hack : version %s\n", RES915_VERSION);

    init_chipset(&map, parameters.forced_chipset);
    // NULL: unknown/non-intel chipset
    init_mode_table(&map);
    init_bios_type(&map);

    if ( ! parameters.quiet )  display_map_info(&map);
    if ( ! map.chipset )  return 3;
    if ( ! map.mode_table )  return 4;
    if ( ! map.bios )  return 5;

    if ( parameters.list )  list_modes(&map);

    parameters_t*  p= &parameters;
    if (p->x == 0 && p->y != 0)  {  p->x= p->y= 0;  get_preferred_res(&p->x, &p->y);  }
    if (p->mode == 0 && p->x != 0 && p->y != 0)  get_last_mode(&map, &p->mode);

    if (p->mode != 0 && p->x != 0 && p->y != 0) {
      vbios_lock(&map, FALSE);
      set_mode(&map, p);
      vbios_lock(&map, TRUE);
      
      if ( ! parameters.quiet )  printf("Patch mode %02x to resolution %dx%d complete\n", p->mode, p->x, p->y);
      if ( parameters.list )  list_modes(&map);
    }

    assert( ! map->unlocked );
    return 0;
}





static grub_err_t
grub_cmd_915resolution (grub_command_t cmd __attribute__ ((unused)), 
			int argc, char *argv[])
{
  return main_915 (argc, argv);
}

static grub_command_t cmd;

GRUB_MOD_INIT(915resolution)
{
  cmd = grub_register_command ("915resolution", grub_cmd_915resolution,
			       "915resolution", "Intel VBE editor");
}

GRUB_MOD_FINI(915resolution)
{
  grub_unregister_command (cmd);
}
